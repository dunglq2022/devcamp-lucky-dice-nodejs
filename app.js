//Khai báo thư viện express
const express = require ('express');
//Khai báo port
const port = 8000;
//Tạo app
const app = express();
//Khai báo path
const path = require ('path');
//Khai báo middile express static
app.use(express.static(path.join(__dirname, 'views')))
//Khai báo express json
app.use(express.json());
//Khai báo mongoose
const mongoose = require('mongoose');
mongoose.set('strictQuery', false)
//Import các router
const {userRouter} = require('./app/Router/userRouter')
const {diceHistoryRouter} = require('./app/Router/diceHistoryRouter')
const {prizeRouter} = require('./app/Router/prizeRouter')
const {voucherRouter} = require('./app/Router/voucherRouter')
const {voucherHistoryRouter} =  require('./app/Router/voucherHistoryRouter')
const {prizeHistoryRouter} =  require('./app/Router/prizeHistoryRouter')
const {diceRouter} = require('./app/Router/diceRouter')

//Connect MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Lucky_Dice_History", (err) => {
    if(err) throw err;

    console.log("Connect MongoDB Successfully!")
})

// app.use('/', (req, res, next) => {
//     let time = new Date;
//     console.log(`ngày ${time.getDate()} tháng ${time.getMonth() + 1} năm ${time.getFullYear()} ${time.getHours()} giờ, ${time.getMinutes()} phút`)
//     next();
// })

// app.use('/', (req, res, next) => {
//     let method = req.method;
//     console.log(method)
//     next();
// })
// app.use('/random-number', (req, res, next) => {
//     let randomNumber = Math.floor(Math.random() * 100);
//     console.log(randomNumber)
//     next();
// })

app.use('/users', userRouter)
app.use('/dice-histories', diceHistoryRouter)
app.use('/prizes', prizeRouter)
app.use('/vouchers', voucherRouter)
app.use('/voucher-histories', voucherHistoryRouter)
app.use('/prize-histories', prizeHistoryRouter)
app.use('/devcamp-lucky-dice', diceRouter)

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/index.html'))
})


app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
})
