const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const diceSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    firtName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        require: true
    },
    userName: {
        type: String,
        require: true
    },
    vouchers: {
        type: mongoose.Types.ObjectId,
        ref: 'voucher',
    },
    dice: {
        type: Number,
        require: true
    },
    prizes: {
        type: mongoose.Types.ObjectId,
        ref: 'prize'
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('dice', diceSchema);