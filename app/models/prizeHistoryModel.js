const mongoose  = require("mongoose");
const Schema = mongoose.Schema;
const prizeHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId,
    user: {
        type: Schema.Types.ObjectId,
        ref: "user"
    },
    prize: {
        type: Schema.Types.ObjectId,
        ref: "prize"
    }
}, {
    timestamps: true
})

module.exports = mongoose.model ('prizeHistory', prizeHistorySchema)