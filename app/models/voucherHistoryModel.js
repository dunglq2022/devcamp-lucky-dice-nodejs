const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const voucherHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    voucher: {
        type: Schema.Types.ObjectId,
        ref: 'voucher'
    }
}, {
    timestamps: true
})

module.exports = mongoose.model ('voucherHistory', voucherHistorySchema)