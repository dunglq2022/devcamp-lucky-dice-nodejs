const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const prizeSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        require: true
    },
    description: {
        type: String,
        require: false
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('prize', prizeSchema)