const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const voucherSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    code: {
        type: String,
        unique: true,
        require: true
    },
    discount: {
        type: Number,
        require: true
    },
    note: {
        type: String,
        require: false
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('voucher', voucherSchema)