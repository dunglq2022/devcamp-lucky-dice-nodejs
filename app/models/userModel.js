//Khai báo mongoose 
const mongoose = require('mongoose');
//Khai báo Schema
const Schema = mongoose.Schema;
//Tạo model
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    userName: {
        type: String,
        required: true,
        unique: true
    },
    firtName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('user', userSchema)