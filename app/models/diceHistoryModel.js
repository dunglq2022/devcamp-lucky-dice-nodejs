//Khai báo mongoose
const mongoose = require ('mongoose');
//Khai báo Schema
const Schema = mongoose.Schema;
//Tạo Model
const diceHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId,
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'user'
    },
    dice: {
        type: Number,
        require: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('diceHistory', diceHistorySchema)