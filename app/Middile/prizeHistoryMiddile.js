const getAllPrizeHistoryMiddile = (req, res, next) => {
    console.log('Get all prize HistoryMiddile')
    next();
}

const getPrizeHistoryMiddileById  = (req, res, next) => {
    console.log('Get prize by id')
    next();
}

const createPrizeHistoryMiddile   = (req, res, next) => {
    console.log('Create prize by id')
    next();
}

const deletePrizeHistoryMiddileById   = (req, res, next) => {
    console.log('Delete prize by id')
    next();
}


const updatePrizeHistoryMiddileById  = (req, res, next) => {
    console.log('Update prize by id')
    next();
}

module.exports = {
    getAllPrizeHistoryMiddile,
    getPrizeHistoryMiddileById,
    createPrizeHistoryMiddile,
    deletePrizeHistoryMiddileById,
    updatePrizeHistoryMiddileById
}

