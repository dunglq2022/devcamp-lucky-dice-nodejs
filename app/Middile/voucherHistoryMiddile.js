const getAllVoucherHistoryMiddile = (req, res, next) => {
    console.log('Get all Voucher HistoryMiddile')
    next();
}

const getVoucherHistoryMiddileById  = (req, res, next) => {
    console.log('Get Voucher by id')
    next();
}

const createVoucherHistoryMiddile   = (req, res, next) => {
    console.log('Create Voucher by id')
    next();
}

const deleteVoucherHistoryMiddileById   = (req, res, next) => {
    console.log('Delete Voucher by id')
    next();
}


const updateVoucherHistoryMiddileById  = (req, res, next) => {
    console.log('Update Voucher by id')
    next();
}

module.exports = {
    getAllVoucherHistoryMiddile,
    getVoucherHistoryMiddileById,
    createVoucherHistoryMiddile,
    deleteVoucherHistoryMiddileById,
    updateVoucherHistoryMiddileById
}

