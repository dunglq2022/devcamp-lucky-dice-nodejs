const getAllDiceMiddile = (req, res, next) => {
    console.log('Get all dice Middile')
    next();
}

const getDiceMiddileById  = (req, res, next) => {
    console.log('Get dice Middile by id')
    next();
}

const createDiceMiddile   = (req, res, next) => {
    console.log('Create dice Middile')
    next();
}

const deleteDiceMiddileById   = (req, res, next) => {
    console.log('Delete dice Middile by id')
    next();
}


const updateDiceMiddileById  = (req, res, next) => {
    console.log('Update dice Middile by id')
    next();
}

module.exports = {
    getAllDiceMiddile,
    getDiceMiddileById,
    createDiceMiddile,
    deleteDiceMiddileById,
    updateDiceMiddileById
}