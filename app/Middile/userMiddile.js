const getAllUserMiddile = (req, res, next) => {
    console.log('Get all user history')
    next();
}

const getUserByIdMiddile = (req, res, next) => {
    console.log('Get user by id')
    next();
}

const createUserMiddile = (req, res, next) => {
    console.log('Create user by id')
    next();
}

const deleteUserByIdMiddile = (req, res, next) => {
    console.log('Delete user by id')
    next();
}


const updateUserByIdMiddile = (req, res, next) => {
    console.log('Update user by id')
    next();
}

module.exports = {
    getAllUserMiddile,
    getUserByIdMiddile,
    createUserMiddile,
    deleteUserByIdMiddile,
    updateUserByIdMiddile
}