const getAllVoucherMiddile = (req, res, next) => {
    console.log('Get all Voucher Middile')
    next();
}

const getVoucherMiddileById  = (req, res, next) => {
    console.log('Get Voucher by id')
    next();
}

const createVoucherMiddile   = (req, res, next) => {
    console.log('Create Voucher by id')
    next();
}

const deleteVoucherMiddileById   = (req, res, next) => {
    console.log('Delete Voucher by id')
    next();
}


const updateVoucherMiddileById  = (req, res, next) => {
    console.log('Update Voucher by id')
    next();
}

module.exports = {
    getAllVoucherMiddile,
    getVoucherMiddileById,
    createVoucherMiddile,
    deleteVoucherMiddileById,
    updateVoucherMiddileById
}

