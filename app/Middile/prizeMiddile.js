const getAllPrizeMiddile = (req, res, next) => {
    console.log('Get all prize Middile')
    next();
}

const getPrizeMiddileById  = (req, res, next) => {
    console.log('Get prize by id')
    next();
}

const createPrizeMiddile   = (req, res, next) => {
    console.log('Create prize by id')
    next();
}

const deletePrizeMiddileById   = (req, res, next) => {
    console.log('Delete prize by id')
    next();
}


const updatePrizeMiddileById  = (req, res, next) => {
    console.log('Update prize by id')
    next();
}

module.exports = {
    getAllPrizeMiddile,
    getPrizeMiddileById,
    createPrizeMiddile,
    deletePrizeMiddileById,
    updatePrizeMiddileById
}

