const getAllDiceHistoryMiddile = (req, res, next) => {
    console.log('Get all dice historyMiddile')
    next();
}

const getDiceHistoryMiddileById  = (req, res, next) => {
    console.log('Get dice historyMiddile by id')
    next();
}

const createDiceHistoryMiddile   = (req, res, next) => {
    console.log('Create dice historyMiddile')
    next();
}

const deleteDiceHistoryMiddileById   = (req, res, next) => {
    console.log('Delete dice historyMiddile by id')
    next();
}


const updateDiceHistoryMiddileById  = (req, res, next) => {
    console.log('Update dice historyMiddile by id')
    next();
}

module.exports = {
    getAllDiceHistoryMiddile,
    getDiceHistoryMiddileById,
    createDiceHistoryMiddile,
    deleteDiceHistoryMiddileById,
    updateDiceHistoryMiddileById
}

