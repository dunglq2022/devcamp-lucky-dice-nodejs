//khai báo mongoose
const mongoose = require('mongoose');
//khai báo model
const userModel = require('../models/userModel')
//Khai báo các function CRUD
const createUser = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    //B2 Validate
    if(!body.userName) {
        return res.status(400).json({
            message: "useName is require"
        })
    }
    if(!body.firtName) {
        return res.status(400).json({
            message: "firtName is require"
        })
    }
    if(!body.lastName) {
        return res.status(400).json({
            message: "lastName is require"
        })
    }
    //B3 Xử lý nghiệp vụ
    let newUser = new userModel({
        _id: mongoose.Types.ObjectId(),
        userName: body.userName,
        firtName: body.firtName,
        lastName: body.lastName,
    })
    userModel.create(newUser, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Create User Fail ${err}`
            })
        } else {
            return res.status(201).json({
                message: `Create User Success ${newUser.userName}`,
                data: data
            })
        }
    })
}

const getAllUser = (req, res) => {
    userModel.find((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Get User Fail ${err}`
            })
        } else {
            return res.status(200).json({
                message: `Get User Success`,
                data: data
            })
        }
    })
}

const getUserById = (req, res) => {
    //B1 Thu thập dữ liệu
    let userId = req.params.userId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "id is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    userModel.findById(userId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Get User Fail ${err}`
            })
        } else if (data == null){
            return res.status(404).json({
                message: `Get User Fail User not found`
            })
        } else {
            return res.status(200).json({
                message: `Get User Success`,
                data: data
            })
        }
    })
}

const updateUserById = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    let userId = req.params.userId;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "id is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    let updateUser = {
        firtName: body.firtName,
        lastName: body.lastName
    }
    userModel.findByIdAndUpdate(userId, updateUser, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Update User Fail ${err}`
            })
        } else {
            return res.status(200).json({
                message: `Update User Success`,
                data: data
            })
        }
    })
}

const deleteUserById = (req, res) => {
    //B1 Thu thập dữ liệu
    let userId = req.params.userId;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "userId is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    userModel.findByIdAndDelete(userId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Delete User Fail ${err}`
            })
        } else {
            return res.status(204).json({
                message: `Delete User Success`,
                data: data
            })
        }
    })
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}