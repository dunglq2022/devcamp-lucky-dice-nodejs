const mongoose = require ('mongoose');
const prizeHistoryModel = require ('../models/prizeHistoryModel')
const createPrizeHistories = (req, res) => {
    let body =  req.body;
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: 'user is valid'
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.prize)) {
        return res.status(400).json({
            message: 'prize is valid'
        })
    }
    let newPrizeHistory = new prizeHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        prize: body.prize,
    })
    prizeHistoryModel.create(newPrizeHistory, (err, prizeHistory) => {
        if(err) {
            return res.status(500).json({
                message: 'Err 500 not create prize history'
            })
        } else {
            return res.status(201).json({
                message: 'Prize history created',
                prizeHistory: prizeHistory
            })
        }
    })
}

const getAllPrizeHistories = (req, res) => {
    let userId = req.query.userId
    if(userId == null) {
        prizeHistoryModel.find((err, prizeHistory) => {
            if(err) {
                return res.status(500).json({
                    message: 'Err 500 not find'
                })
            } else {
                return res.status(200).json({
                    message: 'Prize history found',
                    prizeHistory: prizeHistory
                })
            }
        })
    } else {
        prizeHistoryModel.find({user: userId}).exec((err, data) => {
            if(err) {
                return res.status(500).json({
                    message: 'Err 500 not find'
                })
            } else {
                return res.status(200).json({
                    message: `Prize history found by userId ${userId}`,
                    data
                })
            }
        })
    }
    
}

const getPrizeHistoriesById = (req, res) => {
    let historyId = req.params.historyId;
    if(!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            message: 'history is valid'
        })
    }
    prizeHistoryModel.findById(historyId, (err, prizeHistory) => {
        if(err) {
            return res.status(500).json({
                message: 'Err 500 not find'
            })
        } else {
            if(!prizeHistory){
            return res.status(404).json({
                message: 'Prize history by id not found or deleted on database',
            })
            } else {
                return res.status(200).json({
                    message: 'Prize history by id found',
                    prizeHistory: prizeHistory
                })
            }
        }
    })
}

const updatePrizeHistoriesById = (req, res) => {
    let body = req.body;
    let historyId = req.params.historyId;
    if(!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            message: 'history is valid'
        })
    }
    prizeHistoryModel.findByIdAndUpdate(historyId, body, {new: true}, (err, prizeHistory) => {
        if(err) {
            return res.status(500).json({
                message: 'Err 500 not find'
            })
        } else {
            return res.status(200).json({
                message: 'Prize history by id updated',
                prizeHistory: prizeHistory
            })
        }
    })
}

const deletePrizeHistoriesById = (req, res) => {
    let historyId = req.params.historyId;
    if(!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            message: 'history is valid'
        })
    }
    prizeHistoryModel.findByIdAndDelete(historyId, (err, prizeHistory) => {
        if(err) {
            return res.status(500).json({
                message: 'Err 500 not find'
            })
        } else {
            return res.status(204).json({
                message: 'Prize history by id delete',
                prizeHistory: prizeHistory
            })
        }
    })
}

module.exports = {
    createPrizeHistories,
    getAllPrizeHistories,
    updatePrizeHistoriesById,
    getPrizeHistoriesById,
    deletePrizeHistoriesById
}