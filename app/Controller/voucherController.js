//khai báo mongoose
const mongoose = require('mongoose');
//khai báo model
const voucherModel = require('../models/voucherModel')
//Khai báo các function CRUD
const createVoucher = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    //B2 Validate
    if (!body.code) {
        return res.status(400).json({
            message: 'Code is required'
        })
    }
    if (!body.discount) {
        return res.status(400).json({
            message: 'Discount is required'
        })
    }
    //B3 Xử lý nghiệp vụ
    let newVoucher = new voucherModel({
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note
    })
    voucherModel.create(newVoucher, (err, voucher) => {
        if (err) {
            return res.status(400).json({
                message: `Error add Voucher `
            })
        } else {
            return res.status(201).json({
                message: `Success add Voucher `,
                voucher
            })
        }
    })
}

const getAllVoucher = (req, res) => {
    voucherModel.find((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Get Voucher Fail ${err}`
            })
        } else {
            return res.status(200).json({
                message: `Get Voucher Success`,
                data: data
            })
        }
    })
}

const getVoucherById = (req, res) => {
    //B1 Thu thập dữ liệu
    let voucherId = req.params.voucherId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "Voucher Id is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    voucherModel.findById(voucherId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Get Voucher Fail ${err}`
            })
        } else if (data == null){
            return res.status(404).json({
                message: `Get Voucher null`
            })
        } else {
            return res.status(200).json({
                message: `Get Voucher Success`,
                data: data
            })
        }
    })
}

const updateVoucherById = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    let voucherId = req.params.voucherId;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "Voucher Id is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    let updateVoucherId = {
        discount: body.discount,
        note: body.note
    }
    voucherModel.findByIdAndUpdate(voucherId, updateVoucherId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Update Voucher Fail ${err}`
            })
        } else {
            return res.status(200).json({
                message: `Update Voucher Success`,
                data: data
            })
        }
    })
}

const deleteVoucherById = (req, res) => {
    //B1 Thu thập dữ liệu
    let voucherId = req.params.voucherId;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "VoucherId is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    voucherModel.findByIdAndDelete(voucherId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Delete VoucherId Fail ${err}`
            })
        } else {
            return res.status(204).json({
                message: `Delete VoucherId Success`,
                data: data
            })
        }
    })
}

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}