const mongoose = require('mongoose');
const voucherHistoryModel = require('../models/voucherHistoryModel')
//Các function CRUD
const createVoucherHistory = (req, res) => {
    let body = req.body;
    if (!body.user) {
        res.status(400).send({
            message: "User is required"
        })
    }
    if (!body.voucher) {
        res.status(400).send({
            message: "Voucher is required"
        })
    }
    let newVoucherHistory = new voucherHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        voucher: body.voucher
    })
    voucherHistoryModel.create(newVoucherHistory, (err, voucherHistory) => {
        if (err) {
            res.status(500).json({
                message: "Some error occurred while creating the voucherHistory."
            })
        } else {
            res.status(201).json({
                message: "VoucherHistory created successfully",
                voucherHistory: voucherHistory
            })
        }
    })
}

const getAllVoucherHistory = (req, res) => {
    let userId = req.query.userId;
    if(userId == null) {
        voucherHistoryModel.find((err, voucherHistory) => {
            if (err) {
                res.status(500).json({
                    message: "Some error occurred while retrieving voucherHistory."
                })
            } else {
                res.status(200).json({
                    message: "Successfully retrieved all voucherHistory",
                    voucherHistory: voucherHistory
                })
            }
        })
    } else {
        voucherHistoryModel.find({user: userId}, (err, voucherHistory) => {
            if (err) {
                res.status(500).json({
                    message: "Some error occurred while retrieving voucherHistory."
                })
            } else {
                res.status(200).json({
                    message: `Successfully retrieved all voucherHistory by userId ${userId}`,
                    voucherHistory: voucherHistory
                })
            }
        })
    }
}

const getVoucherHistoryById = (req, res) => {
    let historyId = req.params.historyId;
    voucherHistoryModel.findById(historyId, (err, voucherHistory) => {
        if (err) {
            res.status(500).json({
                message: "Some error occurred while retrieving voucherHistory."
            })
        } else {
            res.status(200).json({
                message: "Successfully retrieved voucherHistory",
                voucherHistory: voucherHistory
            })
        }
    })
}

const updateVoucherHistoryById = (req, res) => {
    let historyId = req.params.historyId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).send(`No voucherHistory with id: ${historyId}`);
    }
    let updateVoucherHistory = {
        user: body.user,
        voucher: body.voucher,
    }
    voucherHistoryModel.findByIdAndUpdate(historyId, updateVoucherHistory, {new: true}, (err, voucherHistory) => {
        if (err) {
            res.status(500).json({
                message: "Some error occurred while updating voucherHistory."
            })
        } else {
            res.status(200).json({
                message: `Update voucher history`,
                voucherHistory: voucherHistory
            })
        }
    })
}

const deleteVoucherHistoryById = (req, res) => {
    let historyId = req.params.historyId;
    if(!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            message: `history Id valid`
        })
    }
    voucherHistoryModel.findByIdAndRemove(historyId, (err, voucherHistory) => {
        if (err) {
            res.status(500).json({
                message: "Some error occurred while deleting voucherHistory."
            })
        } else {
            res.status(204).json({
                message: `voucherHistory deleted successfully`,
                voucherHistory: voucherHistory
            })
        }
    })
}
module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}