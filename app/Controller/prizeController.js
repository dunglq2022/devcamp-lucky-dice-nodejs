//khai báo mongoose
const mongoose = require('mongoose');
//khai báo model
const prizeModel = require('../models/prizeModel')
//Khai báo các function CRUD
const createPrize = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    //B2 Validate
    if (!body.name) {
        return res.status(400).json({
            message: 'Name is required'
        })
    }
    if (!body.description) {
        return res.status(400).json({
            message: 'Description is required'
        })
    }
    //B3 Xử lý nghiệp vụ
    let newPrize = new prizeModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
    })
    prizeModel.create(newPrize, (err, prize) => {
        if (err) {
            return res.status(400).json({
                message: `Error add prize `
            })
        } else {
            return res.status(201).json({
                message: `Success add prize `,
                prize
            })
        }
    })
}

const getAllPrize = (req, res) => {
    prizeModel.find((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Get Prize Fail ${err}`
            })
        } else {
            return res.status(200).json({
                message: `Get Prize Success`,
                data: data
            })
        }
    })
}

const getPrizeById = (req, res) => {
    //B1 Thu thập dữ liệu
    let prizeId = req.params.prizeId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: "prize Id is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    prizeModel.findById(prizeId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Get prize Fail ${err}`
            })
        } else if (data == null){
            return res.status(404).json({
                message: `Get prize null`
            })
        } else {
            return res.status(200).json({
                message: `Get prize Success`,
                data: data
            })
        }
    })
}

const updatePrizeById = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    let prizeId = req.params.prizeId;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: "prize Id is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    let updatePrizeId = {
        description: body.description
    }
    prizeModel.findByIdAndUpdate(prizeId, updatePrizeId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Update prize Fail ${err}`
            })
        } else {
            return res.status(200).json({
                message: `Update prize Success`,
                data: data
            })
        }
    })
}

const deletePrizeById = (req, res) => {
    //B1 Thu thập dữ liệu
    let prizeId = req.params.prizeId;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: "prizeId is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    prizeModel.findByIdAndDelete(prizeId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Delete prizeId Fail ${err}`
            })
        } else {
            return res.status(204).json({
                message: `Delete prizeId Success`,
                data: data
            })
        }
    })
}

module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}