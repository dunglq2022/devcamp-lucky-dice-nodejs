//khai báo mongoose
const mongoose = require('mongoose');
//khai báo model
const diceHistoryModel = require('../models/diceHistoryModel')
//Khai báo các function CRUD
const createDiceHistory = (req, res) => {
    //B1 Thu thập dữ liệu
    let dice = Math.floor(Math.random() * 6) + 1;
    let body = req.body
    console.log(body.user)  
    console.log(dice)
    //B2 Validate
    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `User is valid`
        })
    }
    //B3 Xử lý nghiệp vụ
    let newDiceHistory = new diceHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        dice: dice,
    })
    diceHistoryModel.create(newDiceHistory, (err, diceHistory) => {
        if (err) {
            return res.status(400).json({
                message: `Error add dice history`
            })
        } else {
            return res.status(201).json({
                message: `Success add dice history`,
                diceHistory
            })
        }
    })
}

const getAllDiceHistory = (req, res) => {
    let userId = req.query.userId;
    if (!userId == null) {
        diceHistoryModel.find((err, data) => {
            if (err) {
                return res.status(400).json({
                    message: `Error get all dice history`
                })
            } else {
                return res.status(200).json({
                    message: `Success get all dice history`,
                    data
                })
            }
        })
    } else {
        diceHistoryModel.find({user: userId}).exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    message: `Error get all dice history`
                })
            } else {
                return res.status(200).json({
                    message: `Success get all dice history ${userId}`,
                    data
                })
            }
        })
    }
}

const getDiceHistoryById = (req, res) => {
    //B1 Thu thập dữ liệu
    let diceHistoryId = req.params.diceHistoryId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            message: "dice History Id is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    diceHistoryModel.findById(diceHistoryId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Get dice History Fail ${err}`
            })
        } else if (data == null){
            return res.status(404).json({
                message: `Get dice History Fail`
            })
        } else {
            return res.status(200).json({
                message: `Get dice History Success`,
                data: data
            })
        }
    })
}

const updateDiceHistoryById = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    let diceHistoryId = req.params.diceHistoryId;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            message: "dice History Id is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    let updateDiceHistoryId = {
        dice: body.dice
    }
    diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistoryId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Update dice History Fail ${err}`
            })
        } else {
            return res.status(200).json({
                message: `Update dice History Success`,
                data: data
            })
        }
    })
}

const deleteDiceHistoryById = (req, res) => {
    //B1 Thu thập dữ liệu
    let diceHistoryId = req.params.diceHistoryId;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            message: "dice HistoryId is not valid"
        })
    }
    //B3 Xử lý nghiệp vụ
    diceHistoryModel.findByIdAndDelete(diceHistoryId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Delete dice HistoryId Fail ${err}`
            })
        } else {
            return res.status(204).json({
                message: `Delete dice HistoryId Success`,
                data: data
            })
        }
    })
}

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
}