//khai báo mongoose
const mongoose = require('mongoose');
//khai báo model
const diceModel = require('../models/diceModel')
const userModel = require('../models/userModel');
const diceHistoryModel = require('../models/diceHistoryModel');
const voucherModel = require('../models/voucherModel');
const voucherHistoryModel = require('../models/voucherHistoryModel');
const prizeHistoryModel = require('../models/prizeHistoryModel');
const prizeModel = require('../models/prizeModel')

//Khai báo các function CRUD
const getAllDiceOfUser = (req, res) => {
   username = req.query.username;
   console.log(username)
   userModel.findOne({userName: username}).exec((err, user) => {
    if (err) {
        res.status(500).json({
            message: `Err 500 Server international`,
            err,
        });
    } else {
        console.log(user._id)
        if (user) {
            diceHistoryModel.find({user: user._id}).exec((err, diceHistory) => {
                if (err) {
                    res.status(500).json({
                        message: `Err 500 server international`,
                        err
                    })
                } else {
                    res. status(200).json({
                        message: `Get all dice of user success`,
                        diceHistory
                    })
                }
            })
        } else {
            res.status(200).json({
                message: `User không tồn tại`,
                userId: user
            })
        }
    }
   })
}

const getAllDiceOfUserVoucher = (req, res) => {
    username = req.query.username;
    console.log(username)
    userModel.findOne({userName: username}).exec((err, user) => {
     if (err) {
         res.status(500).json({
             message: `Err 500 Server international`,
             err,
         });
     } else {
         console.log(user._id)
         if (user) {
             voucherHistoryModel.find({user: user._id})
             .populate('voucher')
             .exec((err, voucherHistory) => {
                 if (err) {
                     res.status(500).json({
                         message: `Err 500 server international`,
                         err
                     })
                 } else {
                     res. status(200).json({
                         message: `Get all dice of user success`,
                         voucherHistory
                     })
                 }
             })
         } else {
             res.status(200).json({
                 message: `User không tồn tại`,
                 userId: user
             })
         }
     }
    })
 }

 const getAllDiceOfUserPrize = (req, res) => {
    username = req.query.username;
    console.log(username)
    userModel.findOne({userName: username}).exec((err, user) => {
     if (err) {
         res.status(500).json({
             message: `Err 500 Server international`,
             err,
         });
     } else {
         console.log(user._id)
         if (user) {
             prizeHistoryModel.find({user: user._id})
             .populate('prize')
             .exec((err, prizeHistory) => {
                 if (err) {
                     res.status(500).json({
                         message: `Err 500 server international`,
                         err
                     })
                 } else {
                     res. status(200).json({
                         message: `Get all dice of user success`,
                         prizeHistory
                     })
                 }
             })
         } else {
             res.status(200).json({
                 message: `User không tồn tại`,
                 userId: user
             })
         }
     }
    })
 }

const createDiceOfUse = (req, res) => {
    //B1 Thu thập dữ liệu
    const body = req.body;
    const diceRandom = Math.floor(Math.random() * 6) + 1
    console.log(body)
    console.log(diceRandom)
    //B2 Validate data
    if (!body.firtName) {
        return res.status(400).json({
            message: `Firtname is valid`
        })
    }
    if (!body.lastName) {
        return res.status(400).json({
            message: `Lastname is valid`
        })
    }
    userModel.findOne({userName: body.userName}, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: `Err 500 Bad server`,
                err: err
            })
        } else {
            if (user == null) {
                let newUser = new userModel({
                    _id:mongoose.Types.ObjectId(),
                    userName: body.userName,
                    firtName: body.firtName,
                    lastName: body.lastName,
                })
                userModel.create(newUser, (err, newUser) => {
                    if (err) {
                        return res.status(500).json({
                            message: `Err 500 Bad server`,
                            err
                        })
                    } else {
                        return res.status(201).json({
                            message: `Create new user success`,
                            newUser
                        })
                    }
                })
            } else {
                let newDiceHistory = new diceHistoryModel({
                    _id: mongoose.Types.ObjectId(),
                    user: user._id,
                    dice: diceRandom,
                })
                diceHistoryModel.create(newDiceHistory, (err, newDiceHistory) => {
                    if (err) {
                        return res.status(500).json({
                            message: `Err 500 Bad server`,
                            err
                        }) 
                    } else {
                        if (newDiceHistory.dice <= 3) {
                            let newDice = new diceModel({
                                _id: mongoose.Types.ObjectId(),
                                firtName: body.firtName,
                                lastName: body.lastName,
                                userName: user._id,
                                dice: newDiceHistory.dice,
                                vouchers: null,
                                prize: null
                            })
                            console.log(newDice)
                            diceModel.create(newDice, (err, newDiceLessThan3) => {
                                if (err) {
                                    return res.status(500).json({
                                        message: `Err 500 Bad server`,
                                        err
                                    })
                                } else {
                                    diceHistoryModel.find({user: user._id})
                                    .sort({createdAt: `desc`})
                                    .limit(3)
                                    .exec((err, dice3Near) => {
                                        if (err) {
                                            return res.status(500).json({
                                                message: `Err 500 Bad server dice`,
                                                err
                                            })
                                        } else {
                                            return res.status(200).json({
                                                message: `Dice và 3 lần tung xúc sắc gần nhất`,
                                                dices: newDiceLessThan3,
                                                LanTungXucXacGanNhat: dice3Near
                                            })
                                        }
                                    })
                                }
                            })
                        } else {
                            //Get the count all vouchers
                            voucherModel.count().exec((err, count) => {
                            //Get a random entry
                            let random = Math.floor(Math.random() * count)
                            //Again query all voucher but only fetch one offset by our random #
                            let newDice = new diceModel({
                                _id: mongoose.Types.ObjectId(),
                                firtName: body.firtName,
                                lastName: body.lastName,
                                userName: user._id,
                                dice: newDiceHistory.dice,                                
                            })
                            //Xử lý new dice and voucher history id push [voucher] of dice
                                diceModel.create(newDice, (err, newDice) => {
                                    if (err) {
                                        return res.status(500).json({
                                            message: `Err 500 Bad server new dice`,
                                            err
                                        })
                                    } else {
                                        //Xử lý get random collection and create voucher history
                                        voucherModel.findOne().skip(random).exec((err, voucher) => {
                                            if (err) {
                                                return res.status(500).json({
                                                    message: `Err 500 Bad server voucher`,
                                                    err
                                                })
                                            } else {
                                               //console.log(`Tìm voucher : ${voucher}`)
                                            }
                                            let newVoucherHistory = new voucherHistoryModel({
                                                _id: mongoose.Types.ObjectId(),
                                                voucher: voucher._id,
                                                user: newDice.userName,
                                            })
                                            
                                            voucherHistoryModel.create(newVoucherHistory, (err, newVoucherHistory) => {
                                                if (err) {
                                                    return res.status(500).json({
                                                        message: `Err 500 Bad server new voucher history`,
                                                        err
                                                    })
                                                } else {     
                                                    //console.log(newVoucherHistory)                                                    
                                                    diceModel.findByIdAndUpdate(newDice._id, {$push: {vouchers: voucher._id}}, (err, diceDone) => {
                                                        if (err) {
                                                            return res.status(500).json({
                                                                message: `Err 500 Bad server dice`,
                                                                err
                                                            })
                                                        } else {
                                                            diceHistoryModel.find({user: user._id})
                                                            .sort({createdAt: `desc`})
                                                            .limit(3)
                                                            .exec((err, dice3Near) => {
                                                                if (err) {
                                                                    return res.status(500).json({
                                                                        message: `Err 500 Bad server dice`,
                                                                        err
                                                                    })
                                                                } else {
                                                                    //console.log(diceDone._id) 
                                                                    diceModel.findById(diceDone._id)
                                                                    .populate('vouchers')
                                                                    .exec((err, dices) => {
                                                                        if (err) {
                                                                            return res.status(500).json({
                                                                                message: `Err 500 Bad server dice`,
                                                                                err
                                                                            })
                                                                        } else {
                                                                            
                                                                            //Check nếu 3 dice lớn hơn 3 sẽ cho random prize push vào dice
                                                                            let allCondition = true;
                                                                            dice3Near.forEach((check) => {
                                                                                if (check.dice < 3) {
                                                                                    allCondition = false;
                                                                                }
                                                                            })
                                                                            console.log(`Kiem tra: ${allCondition}`)
                                                                            if(allCondition) {
                                                                                prizeModel.count().exec((err, count) => {
                                                                                    let random = Math.floor(Math.random() * count)
                                                                                    //Again query all voucher but only fetch one offset by our random #
                                                                                    prizeModel.findOne().skip(random).exec((err, prize) => {
                                                                                        //console.log(prize)
                                                                                        if(err) {
                                                                                            return res.status(500).json({
                                                                                                message: `Err 500 Bad server prize`,
                                                                                                err
                                                                                            })
                                                                                        } else {
                                                                                            let newPrizeHistory =  new prizeHistoryModel({
                                                                                                _id: mongoose.Types.ObjectId(),
                                                                                                user: newDice.userName,
                                                                                                prize: prize._id,
                                                                                            })
                                                                                            //console.log(newPrizeHistory)
                                                                                            prizeHistoryModel.create(newPrizeHistory, (err, prizeHistory) => {
                                                                                                if(err) {
                                                                                                    return res.status(500).json({
                                                                                                        message: `Err 500 Bad server prize`,
                                                                                                        err
                                                                                                    })
                                                                                                } else {
                                                                                                    diceModel.findByIdAndUpdate(newDice._id, {$push: {prizes: prize._id}}, (err, diceDone1) => {
                                                                                                        if (err) {
                                                                                                            return res.status(500).json({
                                                                                                                message: `Err 500 Bad server dice`,
                                                                                                                err
                                                                                                            })
                                                                                                        } else {
                                                                                                            diceModel.findById(diceDone1._id)
                                                                                                            .populate('prizes')
                                                                                                            .populate('vouchers')
                                                                                                            .exec((err, dices) => {
                                                                                                                if (err) {
                                                                                                                    return res.status(500).json({
                                                                                                                        message: `Err 500 Bad server dice`,
                                                                                                                        err
                                                                                                                    })
                                                                                                                } else {
                                                                                                                    return res.status(200).json({
                                                                                                                        message: `Dice và 3 lần tung xúc sắc gần nhất`,
                                                                                                                        dices,
                                                                                                                        LanTungXucXacGanNhat: dice3Near
                                                                                                                    })
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                           
                                                                                        }
                                                                                    })
                                                                                })
                                                                            } else {
                                                                                return res.status(200).json({
                                                                                    message: `Dice và 3 lần tung xúc sắc gần nhất`,
                                                                                    dices: dices,
                                                                                    LanTungXucXacGanNhat: dice3Near
                                                                                })
                                                                            }
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        });
                                    }
                                }) 
                            })   
                        }
                    }
                })
            }
        }
    })
}


module.exports = {
    getAllDiceOfUserVoucher,
    createDiceOfUse,
    getAllDiceOfUser,
    getAllDiceOfUserPrize
}