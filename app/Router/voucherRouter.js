const express = require('express');
const voucherRouter =  express.Router();
//Immport các middile
const {
    getAllVoucherMiddile,
    getVoucherMiddileById,
    createVoucherMiddile,
    deleteVoucherMiddileById,
    updateVoucherMiddileById
} = require ('../Middile/voucherMiddile')
//Import các router
const {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
} = require('../Controller/voucherController')

voucherRouter.post('/', createVoucherMiddile, createVoucher)

voucherRouter.get('/',getAllVoucherMiddile, getAllVoucher)

voucherRouter.get('/:voucherId',getVoucherMiddileById, getVoucherById)

voucherRouter.put('/:voucherId',updateVoucherMiddileById, updateVoucherById)

voucherRouter.delete('/:voucherId',deleteVoucherMiddileById, deleteVoucherById)

module.exports = {voucherRouter};