const express = require('express');
const voucherHistoryRouter = express.Router();
//Import Middile
const {
    getAllVoucherHistoryMiddile,
    getVoucherHistoryMiddileById,
    createVoucherHistoryMiddile,
    deleteVoucherHistoryMiddileById,
    updateVoucherHistoryMiddileById
} = require ('../Middile/voucherHistoryMiddile')

//Import Controller
const {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
} = require ('../Controller/voucherHistoryController')

voucherHistoryRouter.post('/',createVoucherHistoryMiddile, createVoucherHistory)
voucherHistoryRouter.get('/',getAllVoucherHistoryMiddile, getAllVoucherHistory)
voucherHistoryRouter.get('/:historyId',getVoucherHistoryMiddileById,
 getVoucherHistoryById)
voucherHistoryRouter.put('/:historyId',updateVoucherHistoryMiddileById,
updateVoucherHistoryById)
voucherHistoryRouter.delete('/:historyId',deleteVoucherHistoryMiddileById,
deleteVoucherHistoryById)
module.exports = {voucherHistoryRouter}