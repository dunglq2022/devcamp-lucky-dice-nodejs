const express = require('express')
const diceRouter = express.Router();
//Import các middile 
const {
    getAllDiceMiddile,
    createDiceMiddile,
} = require('../Middile/diceMiddile')

//Import các controller
const {
    getAllDiceOfUserVoucher,
    createDiceOfUse,
    getAllDiceOfUser,
    getAllDiceOfUserPrize
} = require('../Controller/diceController')

//Tạo route
diceRouter.post('/dice/', createDiceMiddile, createDiceOfUse)
diceRouter.get('/dice-history', getAllDiceMiddile, getAllDiceOfUser)
diceRouter.get('/voucher-history', getAllDiceMiddile, getAllDiceOfUserVoucher)
diceRouter.get('/prize-history', getAllDiceMiddile, getAllDiceOfUserPrize)

module.exports = {diceRouter}