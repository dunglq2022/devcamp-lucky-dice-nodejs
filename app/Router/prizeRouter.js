const express = require ('express');
const prizeRouter = express.Router();
//Import các middile
const {
    getAllPrizeMiddile,
    getPrizeMiddileById,
    createPrizeMiddile,
    deletePrizeMiddileById,
    updatePrizeMiddileById
} = require('../Middile/prizeMiddile')

const {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
} = require('../Controller/prizeController')

prizeRouter.post('/', createPrizeMiddile, createPrize)

prizeRouter.get('/', getAllPrizeMiddile, getAllPrize)

prizeRouter.get('/:prizeId',getPrizeMiddileById, getPrizeById)

prizeRouter.put('/:prizeId',updatePrizeMiddileById,  updatePrizeById)

prizeRouter.delete('/:prizeId', deletePrizeMiddileById, deletePrizeById)

module.exports = {prizeRouter}