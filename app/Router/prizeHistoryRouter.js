const express = require ('express');
const prizeHistoryRouter = express.Router();

//Import các middle
const {
    getAllPrizeHistoryMiddile,
    getPrizeHistoryMiddileById,
    createPrizeHistoryMiddile,
    deletePrizeHistoryMiddileById,
    updatePrizeHistoryMiddileById
} = require ('../Middile/prizeHistoryMiddile')

//Import các controller
const {
    createPrizeHistories,
    getAllPrizeHistories,
    updatePrizeHistoriesById,
    getPrizeHistoriesById,
    deletePrizeHistoriesById
} = require('../Controller/prizeHistoryControler')

prizeHistoryRouter.post('/', createPrizeHistoryMiddile, createPrizeHistories)
prizeHistoryRouter.get('/', getAllPrizeHistoryMiddile, getAllPrizeHistories)
prizeHistoryRouter.get('/:historyId', getPrizeHistoryMiddileById, getPrizeHistoriesById)
prizeHistoryRouter.put('/:historyId', updatePrizeHistoryMiddileById, updatePrizeHistoriesById)
prizeHistoryRouter.delete('/:historyId', deletePrizeHistoryMiddileById, deletePrizeHistoriesById)

module.exports = {prizeHistoryRouter};