const express = require ('express');
var userRouter = express.Router();
//Khai báo các Miidlle
const {
    getAllUserMiddile,
    getUserByIdMiddile,
    createUserMiddile,
    deleteUserByIdMiddile,
    updateUserByIdMiddile
} = require ('../Middile/userMiddile')
//Khai báo các controller
const {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
} = require ('../Controller/userController')

userRouter.get('/', getAllUserMiddile, getAllUser)
userRouter.get('/:userId',getUserByIdMiddile, getUserById)
userRouter.post('/', createUserMiddile, createUser)
userRouter.put('/:userId',updateUserByIdMiddile, updateUserById)
userRouter.delete('/:userId', deleteUserByIdMiddile, deleteUserById)

module.exports = {userRouter};