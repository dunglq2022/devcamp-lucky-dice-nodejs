const express = require ('express');
var diceHistoryRouter = express.Router();
//Khai báo các Miidlle
const {
    getAllDiceHistoryMiddile,
    getDiceHistoryMiddileById,
    createDiceHistoryMiddile,
    deleteDiceHistoryMiddileById,
    updateDiceHistoryMiddileById
} = require ('../Middile/diceHistoryMiddile')
//Khai báo các controller
const {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
} = require ('../Controller/diceHistoryController')
diceHistoryRouter.post('/', createDiceHistoryMiddile, createDiceHistory)

diceHistoryRouter.get('/',getAllDiceHistoryMiddile, getAllDiceHistory)

diceHistoryRouter.get('/:diceHistoryId', getDiceHistoryMiddileById, getDiceHistoryById)

diceHistoryRouter.put('/:diceHistoryId',updateDiceHistoryMiddileById, updateDiceHistoryById)

diceHistoryRouter.delete('/:diceHistoryId',deleteDiceHistoryMiddileById, deleteDiceHistoryById)

module.exports = {diceHistoryRouter};